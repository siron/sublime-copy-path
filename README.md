# Description
This plugin adds functions to copy path info of currently opened file to clipboard such as file full path, file dir path and file filename itself.
 
 
# Usage
All plugin commands can be accessed from the File menu under the "Copy to Clipboard" submenu and also from Command Palette (Ctrl+Shift+P)