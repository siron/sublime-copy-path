import sublime, sublime_plugin, os

class CopyFilePathToClipboardCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        sublime.set_clipboard(self.view.file_name())
        sublime.status_message("Copied file path")

    def is_enabled(self):
        return self.view.file_name() != None and len(self.view.file_name()) > 0

class CopyFilenameToClipboardCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        sublime.set_clipboard(os.path.basename(self.view.file_name()))
        sublime.status_message("Copied filename")

    def is_enabled(self):
        return self.view.file_name() != None and len(self.view.file_name()) > 0

class CopyDirPathToClipboardCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        sublime.set_clipboard(os.path.dirname(self.view.file_name()))
        sublime.status_message("Copied dir path")

    def is_enabled(self):
        return self.view.file_name() != None and len(self.view.file_name()) > 0

